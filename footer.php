<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
 $the_theme = wp_get_theme();
?>

<?php get_sidebar('footerfull'); ?>

<?php
// Page footer
get_template_part( 'loop-templates/content', 'page-footer' );

?>

</div><!-- #page -->

<div class="wrapper wrapper-site-footer" id="wrapper-site-footer">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <footer id="colophon" class="site-footer" role="contentinfo">

                    <div class="site-info">

                        <span>&copy; <?php echo get_bloginfo( 'title' ); ?> <?php printf( __( 'All Rights Reserved', 'understrap' ), 'WordPress' ); ?></span>

                        <a href="https://fxdigital.uk" target="_blank" rel="noopener nofollow">
                            <?php _e('Digital &amp; Site by FX', 'understrap'); ?>
                        </a>

                    </div><!-- .site-info -->

                </footer><!-- #colophon -->

            </div><!--col end -->

        </div><!-- row end -->

    </div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>
