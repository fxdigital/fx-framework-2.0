<?php
/**
 * understrap enqueue scripts
 *
 * @package understrap
 */

$version = '0.1.0';
$GLOBALS['gulp_theme_version'] = $version;

// These are styles and scripts that are shown on the front-end
function understrap_scripts() {
	$version = '0.1.0';
	wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $GLOBALS['gulp_theme_version'] );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $GLOBALS['gulp_theme_version'], true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'understrap_scripts' );

// These are styles and scripts that are shown in the WP back-end
function admin_scripts() {
	wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/css/admin.min.css', array(), $GLOBALS['gulp_theme_version'] );
	wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/admin.min.js', array('jquery'), $GLOBALS['gulp_theme_version'], true );
}
add_action( 'admin_enqueue_scripts', 'admin_scripts' );

// These are styles and scripts that are shared between the front and back-end
function ts_shared_scripts() {
	wp_enqueue_style( 'shared-styles', get_stylesheet_directory_uri() . '/css/shared.min.css', array(), $GLOBALS['gulp_theme_version'] );
	wp_enqueue_script( 'shared-scripts', get_template_directory_uri() . '/js/shared.min.js', array('jquery'), $GLOBALS['gulp_theme_version'], true );
}
add_action( 'wp_enqueue_scripts', 'ts_shared_scripts' );
add_action( 'admin_enqueue_scripts', 'ts_shared_scripts' );


/**
* Enqueue Google Fonts
**/
if( !function_exists("site_fonts") ) {
	function site_fonts() {
		wp_register_style( 'lato', 'https://fonts.googleapis.com/css?family=Lato:300,400,700' );
		wp_enqueue_style( 'lato' );

		// To register custom fonts that are not CDN-hosted, see /fonts/theme/help.md.
	}
}
add_action( 'wp_enqueue_scripts', 'site_fonts' );