<?php
/**
 * Utility functions for PHP, Wordpress that are useful in projects.
 *
 * Usually this was contained in extras.php, but lives here.
 *
 * @package understrap
 */

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

	// Dev utility.

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
	// General utility
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Enhances PHP's print_r() function by adding <pre> tags to format
 *
 * @category DevUtility/GeneralUtility
 *
 * @param mixed $output Something to pass to print_r()
 *
 * @return mixed Prints the supplied entity to the screen, using print_r()
 */
function print_pre( $output ) {
	echo '<pre>';
	print_r( $output );
	echo '</pre>';
}

/**
 * Converts a HEX value to RGB array
 *
 * @category DevUtility/GeneralUtility
 *
 * @param string $color The hex value to convert, with or without #
 *
 * @return array Returns an array of the RGB values
 */
function hex_2_rgb( $color ) {

	// Remove # if present
	$color = str_replace('#', '', $color);

	// Exit if $color is not in 3 or 6 format
	if(3 !== strlen($color) || 6 !== strlen($color))
		return;

	// Convert 3-length colours to 6
	if (strlen($color) == 3) {
	    $color[5] = $color[2];
	    $color[4] = $color[2];
	    $color[3] = $color[1];
	    $color[2] = $color[1];
	    $color[1] = $color[0];
	}

	// Convert colours
    list($r, $g, $b) = array($color[0].$color[1],
                             $color[2].$color[3],
                             $color[4].$color[5]);
    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    // Return RGB array
    return array($r, $g, $b);
}

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
	// Array utilities
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Takes an array of property/ value pairs and echoes them in the HTML attribute format.
 *
 * @category DevUtility/ArrayUtility
 *
 * @param array $properties The array of attribute/ value pairs
 *
 * @return void
 */
function array_to_html_property_pairs( $properties ) {
	foreach ($properties as $property => $value) {
		$value = htmlspecialchars($value);
		echo ' ';
		echo "{$property}=\"{$value}\"";
		echo ' ';
	}
}

/**
 * Adds a key/value pair to the front of an array (by reference)
 *
 * @category DevUtility/ArrayUtility
 *
 * @param array $arr The array of to add to, **passed by reference** to change actual array
 * @param mixed $key The key to add to array
 * @param mixed $value The associated value to pair with $key
 *
 * @return void
 */
function assoc_array_push_front( &$array, $key, $value ) {
    $arr = array_reverse($arr, true);
    $arr[$key] = $value;
    $arr = array_reverse($arr, true);
}

/**
 * Counts all items in array recursively.
 *
 * @category DevUtility/ArrayUtility
 *
 * @param array $arr The parent array to count.
 * @param int $count The number to start the count from.
 *
 * @return int The total count of all arrays.
 */
function count_array_iterative($arr, $count = 0) {

	if(!isset($arr) || !is_array($arr) || 0 === count($arr))
		return 0;

    foreach ($arr as $value) {
        if (is_array($value)) {
            $count = count_array_iterative($value, $count);
        } else {
            $count = $count + 1;
        }
    }
    return $count;
}

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
	// HTTP utilities
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Forms an associative array from the response headers of a HTTP request
 *
 * @category DevUtility/HTTPUtility
 *
 * @param Array $headers The response headers (from $http_response_header)
 *
 * @return Array Associative array
 */
function parse_headers( $headers ) {
    $head = array();
    foreach( $headers as $k=>$v )
    {
        $t = explode( ':', $v, 2 );
        if( isset( $t[1] ) )
            $head[ trim($t[0]) ] = trim( $t[1] );
        else
        {
            $head[] = $v;
            if( preg_match( "#HTTP/[0-9\.]+\s+([0-9]+)#",$v, $out ) )
                $head['reponse_code'] = intval($out[1]);
        }
    }
    return $head;
}

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
	// Text utilities
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
 * *
 * @category DevUtility/TextUtility
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 *
 * @link https://gist.github.com/TimBHowe/6765309
 *
 * @return string Trimmed string.
 */
function truncate_string($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
	if ($considerHtml) {
		// if the plain text is shorter than the maximum length, return the whole text
		if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
			return $text;
		}
		// splits all html-tags to scanable lines
		preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
		$total_length = strlen($ending);
		$open_tags = array();
		$truncate = '';
		foreach ($lines as $line_matchings) {
			// if there is any html-tag in this line, handle it and add it (uncounted) to the output
			if (!empty($line_matchings[1])) {
				// if it's an "empty element" with or without xhtml-conform closing slash
				if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
					// do nothing
				// if tag is a closing tag
				} else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
					// delete tag from $open_tags list
					$pos = array_search($tag_matchings[1], $open_tags);
					if ($pos !== false) {
					unset($open_tags[$pos]);
					}
				// if tag is an opening tag
				} else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
					// add tag to the beginning of $open_tags list
					array_unshift($open_tags, strtolower($tag_matchings[1]));
				}
				// add html-tag to $truncate'd text
				$truncate .= $line_matchings[1];
			}
			// calculate the length of the plain text part of the line; handle entities as one character
			$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
			if ($total_length+$content_length> $length) {
				// the number of characters which are left
				$left = $length - $total_length;
				$entities_length = 0;
				// search for html entities
				if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
					// calculate the real length of all entities in the legal range
					foreach ($entities[0] as $entity) {
						if ($entity[1]+1-$entities_length <= $left) {
							$left--;
							$entities_length += strlen($entity[0]);
						} else {
							// no more characters left
							break;
						}
					}
				}
				$truncate .= substr($line_matchings[2], 0, $left+$entities_length);
				// maximum lenght is reached, so get off the loop
				break;
			} else {
				$truncate .= $line_matchings[2];
				$total_length += $content_length;
			}
			// if the maximum length is reached, get off the loop
			if($total_length>= $length) {
				break;
			}
		}
	} else {
		if (strlen($text) <= $length) {
			return $text;
		} else {
			$truncate = substr($text, 0, $length - strlen($ending));
		}
	}
	// if the words shouldn't be cut in the middle...
	if (!$exact) {
		// ...search the last occurance of a space...
		$spacepos = strrpos($truncate, ' ');
		if (isset($spacepos)) {
			// ...and cut the text in this position
			$truncate = substr($truncate, 0, $spacepos);
		}
	}
	// add the defined ending to the text
	$truncate .= $ending;
	if($considerHtml) {
		// close all unclosed html-tags
		foreach ($open_tags as $tag) {
			$truncate .= '</' . $tag . '>';
		}
	}
	return $truncate;
}

/**
 * Prints a space-delimeted string of array items for use in CSS classes
 *
 * @category DevUtility/TextUtility
 *
 * @param string[] $array The array of strings to print
 *
 * @return string Returns the array items in one string with space inbetween
 */
function the_css_classes_from_array( $array ) {

	/** @param string $return This is a return string container. */
	$return = ' ';

	foreach ($array as $value) {
		$return .= $value.' ';
	}

	echo $return;
}

/**
 * Removes empty <p> with nbsp in them
 *
 * They can't be targeted with :empty in CSS as not technically empty.
 *
 * There is an action hook commented out below to filter the_content through this.
 *
 * @category DevUtility/TextUtility
 *
 * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/the_content Documentation
 *
 * @param string $str This is the_content of the post, from the filter
 *
 * @return string Returns the_content with empty <p> removed
 */
function remove_empty_tags_recursive( $str, $repto = NULL ) {
	// Balances the tags if not already done
	$str = force_balance_tags($str);

	// Exit if string not given or empty.
	if (!is_string ($str) || trim ($str) == '')
		return $str;

	// Recursive empty HTML tags.
	return preg_replace (
		// Pattern written by Junaid Atari.
			'~\s?<p>(\s|&nbsp;)+</p>\s?~',

		// Replace with nothing if string empty.
			!is_string ($repto) ? '' : $repto,

		// Source string
		$str
	);
}
// add_filter('the_content', 'remove_empty_tags_recursive', 20, 1);

/**
 * Shortens URLs for display
 *
 * Removes http, www, etc. to make URLs shorter. Only use for display purposes, these are not valid URLs!
 *
 * @category DevUtility/TextUtility
 *
 * @param string $url This is url to shorten
 *
 * @return string Returns $url with strings removed
 */
function shorten_url( $url ) {
	$url = str_replace( 'http://', '', $url );
	$url = str_replace( 'http://www.', '', $url );
	$url = str_replace( 'https://www.', '', $url );

	return rtrim($url, '/');
}

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

	// WP Utility.

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
	// General utilities
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Get the featured image URL for a post
 *
 * @category WPUtility/GeneralUtility
 *
 * @todo return another size if size given is not set
 *
 * @param Post $post The post object to get featured image URL of
 * @param string $image_size The size of the image URL to return, using the set iamge sizes
 *
 * @return string Returns the URL at the given size
 */
function get_post_image_url( $post = null, $image_size = 'small' ) {

	// If no post given, get the global post
	if(null == $post)
		global $post;

	/** @param mixed[] $image This is the array of URLs for the featured image. */
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $image_size, false );

	if(!empty($image)) {
		return $image[0];
	}

	// Exit if no featured image set
	return;
}

/**
 * Returns results of WP Query for all posts
 *
 * @param int $count The number of posts to query for.
 * @param array $args Any extra args to add to the WP Query.
 * @param array $tax_query a taxonomy query array to use. for eg. for certain post types.
 * @param bool $return_posts Whether to return just the posts or the whole $query.
 *
 * @return array Returns the array of posts from the query (or the $query if !$return_posts)
 */
function get_query_posts( $count = -1, $args = null, $tax_query = array(), $return_posts = true ) {

	// WP_Query arguments
	$defaults = array(
	    'post_type'              => array( 'post' ),
	    'post_status'            => array( 'publish' ),
	    'posts_per_page'         => $count,
	    'ignore_sticky_posts'    => false,
	    'order'                  => 'DESC',
	    'orderby'                => 'date',
	    'tax_query'				 => $tax_query
	);

	// Merge $defaults with given $args taking precedence
	$args = wp_parse_args( $args, $defaults );

	// The Query
	$query = new WP_Query( $args );

	if($query->have_posts()) {
		if($return_posts)
			return $query->posts;
		else return $query;
	} else return;
}

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

	// Taxonomy utilities (incl. tags)

	// Some of these functions utilise the $GLOBALS['disallowed_tags']
	// which is an array to store tags that should not be returned to the front-end.
	// They will likely not interact directly but through check_tag_disallowed(), get_disallowed_tags(), clean_disallowed_tags()

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

$GLOBALS['disallowed_tags'] = array('headline-post');

/**
 * Checks if a given tag is in the disallowed_tags list
 *
 * @category WPUtility/TaxonomyUtility
 *
 * @todo
 *
 * @param WP_Tag|string $tag The Tag object or slug to check
 *
 * @return bool Returns bool of if tag is disallowed
 */
function check_tag_disallowed( $tag ) {
	if(is_a($tag, 'WP_Term')):
		$tag_name = $tag->slug;
	elseif(is_string($tag)):
		$tag_name = $tag;
	else:
		return;
	endif;

	if(in_array($tag_name, $GLOBALS['disallowed_tags']))
		return true;
	else return false;
}

/**
 * Given a tag array it removes any tags in disallowed_tags
 *
 * @category WPUtility/TaxonomyUtility
 *
 * @todo
 *
 * @param WP_Tag[] $tag_array The array of Tags to clean
 *
 * @return WP_Tag[] Returns the cleaned tag array
 */
function clean_disallowed_tags( $tag_array ) {
	if(!is_array($tag_array))
		return null;

	return array_diff($tag_array, $GLOBALS['disallowed_tags']);
}

/**
 * Returns whether post has any tags
 *
 * The $disallowed_tags array defines which tags not to show on the front-end.
 *
 * @category WPUtility/TaxonomyUtility
 *
 * @uses clean_disallowed_tags()
 *
 * @param Post $post The post object to check if has tags
 *
 * @return bool Returns whether post has (valid) tags
 */
function check_has_clean_tags( $post = null ) {

	// If no post given, get the global post
	if(null == $post)
		global $post;

	/** @param object[] $post_tags This is an array of the tags of $post. Returns false if no tags. */
	$post_tags = get_the_tags( $post->ID );

	// Clean tag array of disallowed tags
	$post_tags = clean_disallowed_tags($post_tags);

	// Exit if no tags (it will return false if empty or invalid post)
	if(!is_array($post_tags))
		return false;

	else return true;
}

function get_the_clean_tags( $post = null ) {
	// If no post given, get the global post
	if(null == $post)
		global $post;

	/** @param object[] $post_tags This is an array of the tags of $post. Returns false if no tags. */
	$post_tags = get_the_tags( $post->ID );

	// Exit if no tags (it will return false if empty or invalid post)
	if(!is_array($post_tags))
		return array();

	// Clean tag array of disallowed tags
	$post_tags = clean_disallowed_tags($post_tags);

	return $post_tags;
}

/**
 * Gets the main top-level taxonomy term of post
 *
 * Given a taxonomy, this function returns the Term object of the calculated "main" term.
 *
 * 1. If post only has one top-level term, that is returned.
 * 2. If Yoast is activated, and a primary term is set, that is returned.
 * 3. Else, the most popular by post_count term is returned.
 *
 * @category WPUtility/TaxonomyUtility
 *
 * @param Post $post The post object to get main term of
 * @param string $taxonomy_slug The slug of the taxonomy to get terms of
 *
 * @return Term Returns Term object if $post has a top-level term in this taxonomy
 */
function get_main_taxonomy_term( $post, $taxonomy_slug ) {

	$post_terms = get_the_terms( $post, $taxonomy_slug );

	// Exit if no terms
	if(empty($post_terms) || is_wp_error( $post_terms ))
		return;

	/** @param Term[] $top_level_terms This is an array to store only the top-level terms from $post_terms */
	$top_level_terms = array();

	// Adds only top-level terms to $top_level_terms
	foreach ($post_terms as $term) {
		if(0 === $term->parent)
			array_push($top_level_terms, $term);
	}

	// Exit if no top-level terms
	if(empty($top_level_terms))
		return;

	// 1 - Exit if only one top-level term, returning that term
	if(1 === count($top_level_terms))
		return $top_level_terms[0];

	// 2 - SHOW YOAST PRIMARY CATEGORY if exists and is set
	if( class_exists('WPSEO_Primary_Term') ):
		// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
		$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy_slug, $post->ID );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term = get_term( $wpseo_primary_term );

		// if a primary category is set, return that
		if (!is_wp_error($term))
			return $term;
	endif;

	// 3 - Look through parent terms and pick one with higher post count
	/** @param object $top_parent_term This represents the top-level term with the higher post count. */
	$top_parent_term = $top_level_terms[0];

	// Loop over all top-level terms and replace $top_parent_term if post count is higher
	foreach ($top_level_terms as $term) {
		if($term->count > $top_parent_term->count)
			$top_parent_term = $term;
	}

	return $top_parent_term;
}

/**
 * Prints an <a> with the term's name and link
 *
 * If no term is passed then it get's the main taxonomy term from category for the global post.
 *
 * @category WPUtility/TaxonomyUtility
 *
 * @param Term $term The Term object to get the link for
 *
 * @return void
 */
function the_term_link_anchor( $term = null, $classes = array(), $html_attributes = array() ) {

	if(null == $term) {
		global $post;
		$term = get_main_taxonomy_term($post, 'category');
	}

	if(empty($term))
		return;

	echo '<a href="'.get_term_link( $term, $term->taxonomy ).'" class="'.the_css_classes_from_array($classes).'" '.array_to_html_property_pairs($html_attributes).' >'.$term->name.'</a>';
}

/**
 * Returns the root most term for a given term
 *
 * This will recursively work up the term tree to find a term with $term->parent = 0.
 *
 * @category WPUtility/TaxonomyUtility
 *
 * @param Term $term The Term object to get root term for
 *
 * @return void
 */
function get_root_term($term) {
    $root_term = $term;

    if(0 !== $root_term->parent) {
        $root_term = get_term($root_term->parent);
        $root_term = get_root_term($root_term);
    }

    return $root_term;
}

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
	// User utilities
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Hides the update bar in back-end from non-admins
 *
 * This uses the update_core capability, not the administrator user type.
 *
 * @category WPUtility/UserUtility
 *
 * @return void
 */
function hide_update_notice_to_all_but_admin_users() {
    if(!current_user_can('update_core'))
        remove_action( 'admin_notices', 'update_nag', 3 );
}
// add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
	// Search utilities
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Modify the number of results returned on search results page
 *
 * @category WPUtility/SearchUtility
 *
 * @return void
 */
function change_wp_search_size($queryVars) {
	if ( isset($_REQUEST['s']) )
		$queryVars['posts_per_page'] = 20;
	return $queryVars;
}
add_filter('request', 'change_wp_search_size');

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

	// ACF Utility.

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Reduces the min-height of TinyMCE fields
 *
 * This couldn't be done with the editor-style.css.
 *
 * @category ACFUtility
 *
 * @return void
 */
function acf_wysiwyg_height() {
  echo '<style>
    .acf-editor-wrap iframe {
        min-height: 50px;
    }
  </style>';
}
// add_action('admin_head', 'acf_wysiwyg_height');

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●
// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

	// Understrap functions from extras.php is below.

// ●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●●▬▬▬▬๑۩۩๑▬▬▬▬▬●

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function understrap_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}
	return $classes;
}
add_filter( 'body_class', 'understrap_body_classes' );

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.

add_filter( 'body_class', 'adjust_body_class' );
function adjust_body_class( $classes ) {

    foreach ( $classes as $key => $value ) {
        if ( $value == 'tag' ) unset( $classes[ $key ] );
    }

    return $classes;

}

// Filter custom logo with correct classes
add_filter('get_custom_logo','change_logo_class');
function change_logo_class($html)
{
	$html = str_replace('class="custom-logo"', 'class="img-responsive"', $html);
	$html = str_replace('class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html);
	return $html;
}

// ACF Options page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
