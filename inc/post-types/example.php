<?php
/* Bones Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// let's create the function for the custom type
function example_create() {
	// creating (registering) the custom type
	register_post_type( 'fx_example', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => 'Examples', /* This is the Title of the Group */
			'singular_name' => 'Example', /* This is the individual type */
			'all_items' => 'All Examples', /* the all items menu item */
			'add_new' => 'New Example', /* The add new menu item */
			'add_new_item' => 'New Example', /* Add New Display Title */
			'edit' => 'Edit', /* Edit Dialog */
			'edit_item' => 'Edit Examples', /* Edit Display Title */
			'new_item' => 'New Example', /* New Display Title */
			'view_item' => 'View Example', /* View Display Title */
			'search_items' => 'Search Examples', /* Search Custom Type Title */
			'not_found' =>  'Example not found.', /* This displays if there are no entries yet */
			'not_found_in_trash' => 'No examples found in Trash', /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => 'This is the examples post type', /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-businessman', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'example', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'team', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky', 'page-attributes')
		) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type( 'category', 'example' );

	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type( 'post_tag', 'example' );

}

	// adding the function to the Wordpress init
	add_action( 'init', 'example_create');

	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
/*
	register_taxonomy( 'examples_cat',
		array('example'),
		array('hierarchical' => true,
			'labels' => array(
				'name' => 'Job Categories',
				'singular_name' => 'Job Category',
				'search_items' =>  'Search Job Categories',
				'all_items' => 'All Job Categories',
				'parent_item' => 'Parent Job Category',
				'parent_item_colon' => 'Parent Job Category:',
				'edit_item' => 'Edit Job Category',
				'update_item' => 'Update Job Category',
				'add_new_item' => 'Add New Job Category',
				'new_item_name' => 'New Job Category Name'
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'custom-slug' ),
		)
	);

	register_taxonomy( 'jobs_tag',
		array('example'),
		array('hierarchical' => false,
			'labels' => array(
				'name' => 'Job Tags',
				'singular_name' => 'Job Tag',
				'search_items' =>  'Search Job Tags',
				'all_items' => 'All Job Tags',
				'parent_item' => 'Parent Job Tag',
				'parent_item_colon' => 'Parent Job Tag:',
				'edit_item' => 'Edit Job Tag',
				'update_item' => 'Update Job Tag',
				'add_new_item' => 'Add New Job Tag',
				'new_item_name' => 'New Job Tag Name'
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);
*/

	/*
		looking for custom meta boxes?
		check out this fantastic tool:
		https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
	*/

// Give admin screen order and make sortable
/**
* add order column to admin listing screen for header text
*/
function add_new_fx_example_column($fx_example_columns) {
  $fx_example_columns['menu_order'] = "Order";
  return $fx_example_columns;
}
add_action('manage_edit-fx_example_columns', 'add_new_fx_example_column');

/**
* show custom order column values
*/
function show_order_column($name){
  global $post;

  switch ($name) {
    case 'menu_order':
      $order = $post->menu_order;
      echo $order;
      break;
   default:
      break;
   }
}
add_action('manage_fx_example_posts_custom_column','show_order_column');

/**
* make column sortable
*/
function order_column_register_sortable($columns){
  $columns['menu_order'] = 'menu_order';
  return $columns;
}
add_filter('manage_edit-fx_example_sortable_columns','order_column_register_sortable');

/** 
 * Pre get posts: Menu order
 */ 
function query_menu_order( $query ) {
    if ( is_post_type_archive( 'fx_example' ) ) {
        $query->set( 'orderby', 'menu_order' );
        $query->set( 'order', 'ASC' );
        return;
    }
}
add_action( 'pre_get_posts', 'query_menu_order', 1 );

?>
