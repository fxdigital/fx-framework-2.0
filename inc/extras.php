<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Utility functions from other projects have been places in utilties.php to clear up this file.
 *
 * @package understrap
 */
