<?php
/**
 * Custom shortcodes
 *
 */


/**
 * Outputs a list of social buttons for the active channels from Theme Options. The links are either external links to the channels themselves or can be share buttons.
 *
 * @param string $atts[type] The type of links to output: 	link, share, link-external
 * @param string $atts[style] The color scheme to use: 		colour (color), outline (theme inverse)
 * @param bool $atts[wow] Whether to use wow animation
 * @param bool $atts[helptext] Whether to show "Share on xx" text
 * @param bool $atts[rss] Whether to show the RSS icon
 * @param string $atts[facebook] Use a custom username for Facebook
 * @param string $atts[twitter] Use a custom username for Twitter
 * @param string $atts[linkedin] Use a custom username for LinkedIn
 * @param string $atts[google-plus] Use a custom username for GooglePlus
 * @param string $atts[pinterest] Use a custom username for Pinterest
 * @param bool $atts[comapny] Whether this is a company (adjusts LinkedIn profile URL)
 *
 * @return ul>li>a
 */
function social_links( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'type' => 'link',
			'style' => 'colour',
			'wow' => false,
			'helptext' => false,
			'rss' => false,
			'facebook' => null,
			'twitter' => null,
			'linkedin' => null,
			'google-plus' => null,
			'pinterest' => null,
			'instagram' => null,
			'company' => false
		),
		$atts
	);

	// Set up return string
	$return = '';

	// use Wow
	$wow = '';
	if(true == $atts['wow'])
		$wow = ' wow bounceIn ';

	$return .= '<ul class="social-links '.$atts['type'].' '.$atts['style'].' wow-'.$atts['wow'].'">';


		if(('link' === $atts['type'] && get_field( 'theme_linkedin_username', 'options' )) || 'share' == $atts['type'] || isset($atts['linkedin'])):

			// Generate link and link title
			if('link' === $atts['type']):
				$link_href = 'http://linkedin.com/company/'.get_field( 'theme_linkedin_username', 'options' );
			elseif('link-external' === $atts['type'] && !$atts['company']):
				$link_href = 'http://linkedin.com/in/'.$atts['linkedin'];
			elseif('link-external' === $atts['type'] && $atts['company']):
				$link_href = 'http://linkedin.com/company/'.$atts['linkedin'];
			elseif('share' == $atts['type']):
				$link_href = 'https://www.linkedin.com/shareArticle?mini=true&url='.urlencode(get_permalink()).'&title='.urlencode(get_the_title()).'&source='.urlencode(get_home_url());
			endif;

			$return .= '<li class="li-li '.$wow.'">';
				$return .= '<a class="social-link linkedin" href="'.$link_href.'" target="_blank">';
					$return .= '<i class="fa fa-linkedin" aria-hidden="true"></i>';
				$return .= '</a>';
			$return .= '</li>';
		endif;

		if(('link' === $atts['type'] && get_field( 'theme_twitter_username', 'options' )) || 'share' == $atts['type'] || isset($atts['twitter'])):

			// Generate link and link title
			if('link' === $atts['type']):
				$link_href = 'http://twitter.com/'.get_field( 'theme_twitter_username', 'options' );
			elseif('link-external' === $atts['type']):
				$link_href = 'http://twitter.com/'.$atts['twitter'];
			elseif('share' == $atts['type']):
				$link_message = ''.get_the_title().' - via @'.get_field( 'theme_twitter_username', 'options' );
				$link_message = urlencode(html_entity_decode($link_message, ENT_COMPAT, 'UTF-8'));
				$link_href = 'https://twitter.com/intent/tweet?text='.$link_message.'&url='.urlencode(get_permalink());
			endif;

			$return .= '<li class="tw-li '.$wow.'">';
				$return .= '<a class="social-link twitter" href="'.$link_href.'" target="_blank">';
					$return .= '<i class="fa fa-twitter" aria-hidden="true"></i>';
				$return .= '</a>';
			$return .= '</li>';
		endif;

		if(('link' === $atts['type'] && get_field( 'theme_facebook_username', 'options' )) || 'share' == $atts['type'] || isset($atts['facebook'])):

			// Generate link and link title
			if('link' === $atts['type']):
				$link_href = 'http://facebook.com/'.get_field( 'theme_facebook_username', 'options' );
			elseif('link-external' === $atts['type']):
				$link_href = 'http://facebook.com/'.$atts['facebook'];
			elseif('share' == $atts['type']):
				$link_href = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode(get_permalink());
			endif;

			// $return .= $li_open;
			$return .= '<li class="fb-li '.$wow.'">';
				$return .= '<a class="social-link facebook" href="'.$link_href.'" target="_blank">';
					$return .= '<i class="fa fa-facebook" aria-hidden="true"></i>';
				$return .= '</a>';
			$return .= '</li>';
		endif;

		if(('link' === $atts['type'] && get_field( 'theme_google-plus_username', 'options' )) || 'share' == $atts['type'] || isset($atts['google-plus'])):

			// Generate link and link title
			if('link' === $atts['type']):
				$link_href = 'http://plus.google.com/+'.get_field( 'theme_google-plus_username', 'options' );
			elseif('link-external' === $atts['type']):
				$link_href = 'http://plus.google.com/+'.$atts['google-plus'];
			elseif('share' == $atts['type']):
				$link_href = 'https://plus.google.com/share?url='.urlencode(get_permalink());
			endif;

			$return .= '<li class="go-li '.$wow.'">';
				$return .= '<a class="social-link google" href="'.$link_href.'" target="_blank">';
					$return .= '<i class="fa fa-google-plus" aria-hidden="true"></i>';
				$return .= '</a>';
			$return .= '</li>';
		endif;

		if(('link' === $atts['type'] && get_field( 'theme_pinterest_username', 'options' )) || 'share' == $atts['type'] || isset($atts['pinterest'])):

			// Generate link and link title
			if('link' === $atts['type']):
				$link_href = 'http://pinterest.com/'.get_field( 'theme_pinterest_username', 'options' );
			elseif('link-external' === $atts['type']):
				$link_href = 'http://pinterest.com/'.$atts['pinterest'];
			elseif('share' == $atts['type']):
				$link_href = 'https://www.pinterest.com/shareArticle?mini=true&url='.urlencode(get_permalink()).'&title='.urlencode(get_the_title()).'&source='.urlencode(get_home_url());
			endif;

			$return .= '<li class="pi-li '.$wow.'">';
				$return .= '<a class="social-link pinterest" href="'.$link_href.'" target="_blank">';
					$return .= '<i class="fa fa-pinterest" aria-hidden="true"></i>';
				$return .= '</a>';
			$return .= '</li>';
		endif;

		if(('link' === $atts['type'] && get_field( 'theme_instagram_username', 'options' )) || isset($atts['instagram'])):

			// Generate link and link title
			if('link' === $atts['type']):
				$link_href = 'http://instagram.com/'.get_field( 'theme_instagram_username', 'options' );
			elseif('link-external' === $atts['type']):
				$link_href = 'http://instagram.com/'.$atts['instagram'];
			elseif('share' == $atts['type']):
				$link_href = 'https://www.instagram.com/shareArticle?mini=true&url='.urlencode(get_permalink()).'&title='.urlencode(get_the_title()).'&source='.urlencode(get_home_url());
			endif;

			$return .= '<li class="in-li '.$wow.'">';
				$return .= '<a class="social-link instagram" href="'.$link_href.'" target="_blank">';
					$return .= '<i class="fa fa-instagram" aria-hidden="true"></i>';
				$return .= '</a>';
			$return .= '</li>';
		endif;

		if(true == $atts['rss']):
			$link_href = get_feed_link();
			$return .= '<li class="rs-li '.$wow.'">';
				$return .= '<a class="social-link rss" href="'.$link_href.'" target="_blank">';
					$return .= '<i class="fa fa-rss" aria-hidden="true"></i>';
				$return .= '</a>';
			$return .= '</li>';
		endif;

		if('true' == $atts['helptext'])
			$return .= '<li class="text'.$wow.'"><span></span></li>';


	$return .= '</ul>';

	return $return;

}
add_shortcode( 'social', 'social_links' );


/**
 * Outputs a button as an anchor.
 *
 * @param string 	$atts[style]			The style of the button: 			solid, ghost
 * @param string 	$atts[link]				The url to link the button from
 * @param string 	$atts[target]			The target of the link
 * @param string 	$atts[color]			The color theme of the link:		primary, inverse, white
 * @param string	$atts[icon]				The FA class for the first icon
 * @param string	$atts[icon-two]			The FA class for the second icon
 * @param string	$atts[class]			Any classes to add to button
 * @param string	$atts[id]				String to set the ID of button
 * @param bool		$atts[button_wrap]		Whether to use <button> over <a>
 * @param string	$atts[hover_text]		String to show when button is hovered (animates in/out)
 * @param bool		$atts[ajax]				Whether this button will use AJAX (includes spinner/ progress layer)
 * @param string	$atts[data-toggle]		String to set for the data-toggle property
 * @param string	$atts[aria-expanded]	String to set for the aria-expanded property
 * @param string	$atts[aria-controls]	String to set for the aria-controls property
 *
 * @return a.button
 */
function button( $atts , $content = null ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'style' => 'solid',
			'link' => '#',
			'target' => '_self',
			'color' => 'white',
			'icon' => null,
			'icon-two' => null,
			'class' => '',
			'id' => '',
			'button_wrap' => false,
			'hover_text' => null,
			'ajax' => false,
			'data-toggle' => null,
			'data-modalID' => null,
			'aria-expanded' => null,
			'aria-controls' => null,
			'has_mobile_variant' => null,
			'mobile_variant_text' => null
		),
		$atts
	);

	$return = '';

	$hover_text = 'false';
	if($atts['hover_text'])
		$hover_text = 'true';

	$link_properties = '';
	if(isset($atts['data-toggle']))
		$link_properties .= ' data-toggle="'.$atts['data-toggle'].'"';
	if(isset($atts['data-modalID']))
		$link_properties .= ' data-modalID="'.$atts['data-modalID'].'"';
	if(isset($atts['aria-expanded']))
		$link_properties .= ' aria-expanded="'.$atts['aria-expanded'].'"';
	if(isset($atts['aria_controls']))
		$link_properties .= ' aria_controls="'.$atts['aria_controls'].'"';

	if(isset($atts['has_mobile_variant'])){
		$atts['class'] .= ' has-mobile-variant ';
	}

	if($atts['button_wrap'])
		$return .= '<button id="'.$atts['id'].'" class="button button-tag '.$atts['class'].' button-'.$atts['style'].' button-'.$atts['color'].' button-hover-'.$hover_text.'" data-modalID="'.$atts['data-modalID'].'" type="submit" '.$link_properties.'><span>';
	else
		$return .= '<a id="'.$atts['id'].'" class="button '.$atts['class'].' button-'.$atts['style'].' button-'.$atts['color'].' button-hover-'.$hover_text.'" href="'.$atts['link'].'" target="'.$atts['target'].'" '.$link_properties.'>';

		if($atts['hover_text']) {
			$return .= '<span class="content first">'.$content.'</span>';
			$return .= '<span class="content second">'.$atts['hover_text'].'</span>';
		} elseif($atts['has_mobile_variant']) {
			$return .= '<span class="content medium-up">'.$content.'</span>';
			$return .= '<span class="content joy-phone">'.$atts['mobile_variant_text'].'</span>';
		} else {
			$return .= '<span class="content">'.$content.'</span>';
		}

		if($atts['icon'])
			$return .= '<i class="button-icon icon-one fa-fw fa '.$atts['icon'].'" aria-hidden="true"></i>';
		if($atts['icon-two'])
			$return .= '<i class="button-icon icon-two fa-fw fa '.$atts['icon-two'].'" aria-hidden="true"></i>';

		if($atts['ajax'])
			$return .= '<div class="loading-ajax"><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></div>';

	if($atts['button_wrap'])
		$return .= '</span></button>';
	else
		$return .= '</a>';

	return $return;

}
add_shortcode( 'button', 'button' );


?>