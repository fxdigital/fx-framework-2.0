<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php require_once('ascii.php'); ?>

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
<meta name=“theme-color” content=“#000000” />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png"/>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php
wp_nav_menu(
        array(
            'theme_location' => 'mobile',
            'container_class' => 'off-canvas-menu',
            'menu_class' => 'nav',
            'fallback_cb' => '',
            'menu_id' => 'mobile',
            'walker' => new wp_bootstrap_navwalker()
        )
);
?>

<div id="site" class="hfeed theme-site">

    <!-- ******************* The Navbar Area ******************* -->
    <div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

        <a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

        <nav class="navbar navbar-dark bg-primary site-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">


                <div class="container">


                            <div class="navbar-header">

                                <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->

                                <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target=".exCollapsingNavbar" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
    					&#9776;
  				</button>

                                <!-- Your site title as branding in the menu -->
	                                <?php if (!has_custom_logo()) { ?>
		                                <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
		                                	<?php bloginfo( 'name' ); ?>
		                                </a>
	                                <?php } else { the_custom_logo(); } ?><!-- end custom logo -->

                            </div>

                            <!-- The WordPress Menu goes here -->
                            <?php wp_nav_menu(
                                    array(
                                        'theme_location' => 'primary',
                                        'container_class' => 'collapse navbar-toggleable-xs exCollapsingNavbar',
                                        'menu_class' => 'nav navbar-nav',
                                        'fallback_cb' => '',
                                        'menu_id' => 'main-menu',
                                        'walker' => new wp_bootstrap_navwalker()
                                    )
                            ); ?>

                </div> <!-- .container -->

        </nav><!-- .site-navigation -->

    </div><!-- .wrapper-navbar end -->

    <!-- This is a wrapper for the page. it is closed after the page footer, before the site footer -->
    <div id="page" class="theme-page">

<!-- Page header (if set in ACF) -->
<?php
    // $page_id = get_queried_object_id();

    // if('none' !== get_field('header_type', $page_id) || null !== get_field('header_type', $page_id)) {
    //     get_template_part( 'loop-templates/content', 'page-header' );
    // }
    // if('none' == get_field('header_type', $page_id)  || null == get_field('header_type', $page_id)) {
    //     echo '<div class="fixed-spacer"></div>';
    // }
?>
