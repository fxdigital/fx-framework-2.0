// FX Framework 2.0 gulp file
// @version: 	2.3.0
// @author: 	Clayton Jones
// @email: 		<clayton@wearefx.co.uk>
// @company: 	FX Digital
// @date: 		13/01/2017
// @lastupdate:	28/11/2017

/////////////////////////////////

// Defining base pathes
var basePaths = {
    bower: './bower_components/',
    node: './node_modules/',
    installedBower: './src/',
    cssCompiled: './css/',
    js: './js/',
    dist: './dist/',
    fonts: './fonts/'
};

// This should be auto set when project is created
var browserSyncProxyURL = "localhost/set_path_here/";

var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var del = require('del');
	var vinylPaths = require('vinyl-paths');
var bump = require('gulp-bump');
var replace = require('gulp-replace');
var runSequence = require('run-sequence');
var fs = require('fs');

// getting sassy
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var sourcemaps = require('gulp-sourcemaps');

// hello world
var notify = require('gulp-notify');
var os = require('os');
var open = require('gulp-open');
	var editor_app = '/Applications/Sublime\ Text.app';
	if('win32' === os.platform())
		editor_app = 'C:\Program Files\Sublime Text 3';

// linting
var cache = require('gulp-cached');
var scsslint = require('gulp-scss-lint');
var jshint = require('gulp-jshint');
	var stylish = require('jshint-stylish');

// browser sync
var browserSync = require('browser-sync').create();
var useBrowserSync = true;
var reload = browserSync.reload;
	var browserSyncWatchFiles = [
	    './css/*.min.css',
	    './js/*.min.js',
	    './**/*.php'
	];
	var browserSyncOptions = {
	    proxy: browserSyncProxyURL,
	    notify: false,
	    open: false
	};

// bower + scripts
var flatten = require('gulp-flatten');
var gulpFilter = require('gulp-filter');
var uglify = require('gulp-uglify');
var mainBowerFiles = require('main-bower-files');


// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('sass', ['sass:minify']);

	gulp.task('sass:compile', ['lint:sass'], function () {

		var onError = function(err) {

	    	notify.onError({
	    		title:    "SASS did not compile",
	    		subtitle: "<%= error.relativePath %>",
	    		message:  "Error: <%= error.messageOriginal %>",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/scss.png',
	    		wait: true,
	    		open: "<%= error.file %>",
	    		onLast: true
	    	})(err);

	    	notify.on('click', function (notifierObject, options) {
	    		gulp.src(notifierObject.open)
	    			.pipe(open({
	    				app: editor_app,
	    			}));
	    	});

			this.emit('end');
		};

		return gulp.src([
				// Bower css but not maps
				basePaths.installedBower + 'css/**/*.css',
				'!'+basePaths.installedBower + 'css/**/*.map',
				'!'+basePaths.installedBower + 'css/**/*.min.css',

				// Tether
				'./sass/tether/**/*.css',
				'!./sass/tether/**/*.min.css',

				// sass file that imports all custom sass
				'./sass/theme.scss'
			])

		   .pipe(plumber({
				errorHandler: onError
			}))

		   // init sourcemaps
		   .pipe(sourcemaps.init({loadMaps: true}))


		   // if lint passes, then sass is checked and built
		   .pipe(sass({
				style: 'compact'
			}))

		   // autoprefix now
		   .pipe(autoprefixer())

		   // concat
		   .pipe(concat('theme.css'))

		   // make the source maps
		   .pipe(sourcemaps.write('.'))

		   // output the built sass
		   .pipe(gulp.dest(basePaths.cssCompiled));
	});

	gulp.task('sass:minify', ['clean:sass', 'sass:compile'], function () {

		return gulp.src([basePaths.cssCompiled+'theme.css'])
			.pipe(plumber())
			.pipe(sourcemaps.init({loadMaps: true}))
			.pipe(rename({suffix: '.min'}))
			.pipe(csso())
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('./css'))
			.pipe(notify({
			   title: 'Sass sucessfully compiled',
			   message: 'The styles are compiled and minified.',
			   sound: "Pop",
			   icon: './config/gulp/images/pass.png',
			   contentImage: './config/gulp/images/scss.png',
			   onLast: true
			}))
			.pipe(browserSync.stream({match: '**/*.css'}));
	});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('sass-admin', ['sass-admin:minify']);

	gulp.task('sass-admin:compile', ['lint:sass-admin'], function () {

		var onError = function(err) {

	    	notify.onError({
	    		title:    "Admin SASS did not compile",
	    		subtitle: "<%= error.relativePath %>",
	    		message:  "Error: <%= error.messageOriginal %>",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/scss.png',
	    		wait: true,
	    		open: "<%= error.file %>",
	    		onLast: true
	    	})(err);

	    	notify.on('click', function (notifierObject, options) {
	    		gulp.src(notifierObject.open)
	    			.pipe(open({
	    				app: editor_app,
	    			}));
	    	});

			this.emit('end');
		};

		return gulp.src([
				// Bower css but not maps
				// basePaths.installedBower + 'css/**/*.css',
				// '!'+basePaths.installedBower + 'css/**/*.map',
				// '!'+basePaths.installedBower + 'css/**/*.min.css',

				// // Tether
				// './sass/tether/**/*.css',
				// '!./sass/tether/**/*.min.css',

				// sass file that imports all custom sass
				'./sass/admin.scss'
			])

		   .pipe(plumber({
				errorHandler: onError
			}))

		   // init sourcemaps
		   .pipe(sourcemaps.init({loadMaps: true}))


		   // if lint passes, then sass is checked and built
		   .pipe(sass({
				style: 'compact'
			}))

		   // autoprefix now
		   .pipe(autoprefixer())

		   // concat
		   .pipe(concat('admin.css'))

		   // make the source maps
		   .pipe(sourcemaps.write('.'))

		   // output the built sass
		   .pipe(gulp.dest(basePaths.cssCompiled));
	});

	gulp.task('sass-admin:minify', ['clean:sass-admin', 'sass-admin:compile'], function () {

		return gulp.src([basePaths.cssCompiled+'admin.css'])
			.pipe(plumber())
			.pipe(sourcemaps.init({loadMaps: true}))
			.pipe(rename({suffix: '.min'}))
			.pipe(csso())
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('./css'))
			.pipe(notify({
			   title: 'Admin Sass sucessfully compiled',
			   message: 'The admin styles are compiled and minified.',
			   sound: "Pop",
			   icon: './config/gulp/images/pass.png',
			   contentImage: './config/gulp/images/scss.png',
			   onLast: true
			}));
			// .pipe(browserSync.stream({match: '**/*.css'}));
	});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('sass-shared', ['sass-shared:minify']);

	gulp.task('sass-shared:compile', ['lint:sass-shared'], function () {

		var onError = function(err) {

	    	notify.onError({
	    		title:    "Shared SASS did not compile",
	    		subtitle: "<%= error.relativePath %>",
	    		message:  "Error: <%= error.messageOriginal %>",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/scss.png',
	    		wait: true,
	    		open: "<%= error.file %>",
	    		onLast: true
	    	})(err);

	    	notify.on('click', function (notifierObject, options) {
	    		gulp.src(notifierObject.open)
	    			.pipe(open({
	    				app: editor_app,
	    			}));
	    	});

			this.emit('end');
		};

		return gulp.src([
				// Bower css but not maps
				// basePaths.installedBower + 'css/**/*.css',
				// '!'+basePaths.installedBower + 'css/**/*.map',
				// '!'+basePaths.installedBower + 'css/**/*.min.css',

				// // Tether
				// './sass/tether/**/*.css',
				// '!./sass/tether/**/*.min.css',

				// sass file that imports all custom sass
				'./sass/shared.scss'
			])

		   .pipe(plumber({
				errorHandler: onError
			}))

		   // init sourcemaps
		   .pipe(sourcemaps.init({loadMaps: true}))


		   // if lint passes, then sass is checked and built
		   .pipe(sass({
				style: 'compact'
			}))

		   // autoprefix now
		   .pipe(autoprefixer())

		   // concat
		   .pipe(concat('shared.css'))

		   // make the source maps
		   .pipe(sourcemaps.write('.'))

		   // output the built sass
		   .pipe(gulp.dest(basePaths.cssCompiled));
	});

	gulp.task('sass-shared:minify', ['clean:sass-shared', 'sass-shared:compile'], function () {

		return gulp.src([basePaths.cssCompiled+'shared.css'])
			.pipe(plumber())
			.pipe(sourcemaps.init({loadMaps: true}))
			.pipe(rename({suffix: '.min'}))
			.pipe(csso())
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('./css'))
			.pipe(notify({
			   title: 'Shared Sass sucessfully compiled',
			   message: 'The shared styles are compiled and minified.',
			   sound: "Pop",
			   icon: './config/gulp/images/pass.png',
			   contentImage: './config/gulp/images/scss.png',
			   onLast: true
			}))
			.pipe(browserSync.stream({match: '**/*.css'}));
	});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('watch', ['browser-sync'], function () {
	// Upon saving a scss file, sass will be called, which first lints
	gulp.watch('./sass/theme/**/*.scss', ['sass']);
	gulp.watch('./sass/theme.scss', ['sass']);

	// The same again for the admin files
	gulp.watch('./sass/admin/**/*.scss', ['sass-admin']);
	gulp.watch('./sass/admin.scss', ['sass-admin']);

	// The same again for the shared files
	gulp.watch('./sass/shared/**/*.scss', ['sass-shared']);
	gulp.watch('./sass/shared.scss', ['sass-shared']);

	// Watches all custom js files
	gulp.watch('./js/theme/*.js', ['scripts']);
	gulp.watch('./js/admin/*.js', ['scripts-admin']);
	gulp.watch('./js/shared/*.js', ['scripts-shared']);

	// Wacthes all PHP files
	gulp.watch('*.php');
});

gulp.task('browser-sync', function() {
    browserSync.init(browserSyncWatchFiles, browserSyncOptions);
});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('bower', ['bower:install']);

// grab libraries files from bower_components, minify and push in /src
gulp.task('bower:install', ['clean:bower'], function() {
	// Check if bower_components exists before running the process
	if (!fs.existsSync(basePaths.bower)) {
		return;
	}

	var jsFilter = gulpFilter(['**/*.js', '!**/*.min.js'], {restore: true}),
		cssFilter = gulpFilter('**/*.css', {restore: true}),
		fontFilter = gulpFilter(['**/*.eot', '**/*.woff', '**/*.svg', '**/*.ttf', '**/*.otf', '**/*.woff2'], {restore: true});

	// return gulp.src(mainBowerFiles())
	return gulp.src(mainBowerFiles(/* options */), { base: basePaths.bower })

	// grab vendor js files from bower_components, minify and push in /src
	.pipe(jsFilter)
	.pipe(gulp.dest(basePaths.installedBower + '/js/'))
	.pipe(jsFilter.restore)

	// grab vendor css files from bower_components, minify and push in /src
	.pipe(cssFilter)
	.pipe(gulp.dest(basePaths.installedBower + '/css'))
	.pipe(cssFilter.restore)

	// grab vendor font files from bower_components and push in /src
	.pipe(fontFilter)
	.pipe(flatten())
	.pipe(gulp.dest(basePaths.installedBower + '/fonts'))
	.pipe(gulp.dest(basePaths.fonts));
});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('scripts', ['scripts:minify']);

	// This will grab all of the understrap required (incl. bootstrap) js, then the bower js, then our own "custom.js" (theme.js)
	gulp.task('scripts:compile', ['lint:scripts'], function () {
		return gulp.src([
		  // All BS4 js (using the bundle version which includes Popper.js, this also replaces Tether.js)
		  basePaths.js + 'bootstrap4/bootstrap.bundle.js',

		  // All understrap js
		  // basePaths.js + 'understrap/**/*.js',

		  // All bower packages js
		  basePaths.installedBower + '/js/**/*.js',

		  // No mins or maps
		  '!'+basePaths.installedBower + 'js/**/*.min.js',
		  '!'+basePaths.installedBower + 'js/**/*.js.map',

		  // "custom.js"
		  basePaths.js + 'theme/*.js'
		])
		.pipe(plumber())
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(concat('theme.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(basePaths.js));
	});

		gulp.task('scripts:minify', ['clean:scripts', 'scripts:compile'], function () {
			return gulp.src(basePaths.js + 'theme.js')
			.pipe(plumber())
			.pipe(sourcemaps.init({loadMaps: true}))
			.pipe(uglify())
			.pipe(rename({
			  suffix: ".min"
			}))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(basePaths.js))
			.pipe(notify({
			   title: 'Scripts sucessfully compiled',
			   message: 'The scripts are compiled and minified.',
			   sound: "Pop",
			   icon: './config/gulp/images/pass.png',
			   contentImage: './config/gulp/images/js.png',
			   onLast: true
			}))
			.pipe(browserSync.reload({stream:true}));
		});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('scripts-admin', ['scripts-admin:minify']);

	// This will grab all of the understrap required (incl. bootstrap) js, then the bower js, then our own "custom.js" (theme.js)
	gulp.task('scripts-admin:compile', ['lint:scripts-admin'], function () {
		return gulp.src([
		  // "custom.js"
		  basePaths.js + 'admin/*.js'
		])
		.pipe(plumber())
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(concat('admin.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(basePaths.js));
	});

		gulp.task('scripts-admin:minify', ['clean:scripts-admin', 'scripts-admin:compile'], function () {
			return gulp.src(basePaths.js + 'admin.js')
			.pipe(plumber())
			.pipe(sourcemaps.init({loadMaps: true}))
			.pipe(uglify())
			.pipe(rename({
			  suffix: ".min"
			}))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(basePaths.js))
			.pipe(notify({
			   title: 'Admin scripts sucessfully compiled',
			   message: 'The admin scripts are compiled and minified.',
			   sound: "Pop",
			   icon: './config/gulp/images/pass.png',
			   contentImage: './config/gulp/images/js.png',
			   onLast: true
			}));
			// .pipe(browserSync.reload({stream:true}));
		});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('scripts-shared', ['scripts-shared:minify']);

	// This will grab all of the understrap required (incl. bootstrap) js, then the bower js, then our own "custom.js" (theme.js)
	gulp.task('scripts-shared:compile', ['lint:scripts-shared'], function () {
		return gulp.src([
		  // "custom.js"
		  basePaths.js + 'shared/*.js'
		])
		.pipe(plumber())
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(concat('shared.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(basePaths.js));
	});

		gulp.task('scripts-shared:minify', ['clean:scripts-shared', 'scripts-shared:compile'], function () {
			return gulp.src(basePaths.js + 'shared.js')
			.pipe(plumber())
			.pipe(sourcemaps.init({loadMaps: true}))
			.pipe(uglify())
			.pipe(rename({
			  suffix: ".min"
			}))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(basePaths.js))
			.pipe(notify({
			   title: 'Shared scripts sucessfully compiled',
			   message: 'The shared scripts are compiled and minified.',
			   sound: "Pop",
			   icon: './config/gulp/images/pass.png',
			   contentImage: './config/gulp/images/js.png',
			   onLast: true
			}))
			.pipe(browserSync.reload({stream:true}));
		});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('lint', ['lint:sass', 'lint:sass-admin', 'lint:sass-shared', 'lint:scripts', 'lint:scripts-admin', 'lint:scripts-shared']);

	gulp.task('lint:sass', function () {

		var onError = function(err) {
	    	notify.onError({
	    		title:    "SASS Linter",
	    		subtitle: "Some files didn\'t pass the lint check",
	    		message:  "Check your Terminal to see the errors.",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/scss.png',
	    		wait: true,
	    		onLast: true
	    	})(err);
		};

		return gulp.src(['./sass/theme/**/*.scss', './sass/theme.scss'])
			.pipe(plumber({
				errorHandler: onError
			}))
			// .pipe(cache('sass-linting'))
			.pipe(scsslint({
				config: './config/gulp/linters/sass-lint.yml',
			}))
			.pipe(scsslint.failReporter());
	});

	gulp.task('lint:sass-admin', function () {

		var onError = function(err) {
	    	notify.onError({
	    		title:    "Admin SASS Linter",
	    		subtitle: "Some files didn\'t pass the lint check",
	    		message:  "Check your Terminal to see the errors.",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/scss.png',
	    		wait: true,
	    		onLast: true
	    	})(err);
		};

		return gulp.src(['./sass/admin/**/*.scss', './sass/admin.scss'])
			.pipe(plumber({
				errorHandler: onError
			}))
			// .pipe(cache('sass-linting'))
			.pipe(scsslint({
				config: './config/gulp/linters/sass-lint.yml',
			}))
			.pipe(scsslint.failReporter());
	});

	gulp.task('lint:sass-shared', function () {

		var onError = function(err) {
	    	notify.onError({
	    		title:    "Shared SASS Linter",
	    		subtitle: "Some files didn\'t pass the lint check",
	    		message:  "Check your Terminal to see the errors.",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/scss.png',
	    		wait: true,
	    		onLast: true
	    	})(err);
		};

		return gulp.src(['./sass/shared/**/*.scss', './sass/shared.scss'])
			.pipe(plumber({
				errorHandler: onError
			}))
			// .pipe(cache('sass-linting'))
			.pipe(scsslint({
				config: './config/gulp/linters/sass-lint.yml',
			}))
			.pipe(scsslint.failReporter());
	});

	gulp.task('lint:scripts', function () {

		var onError = function(err) {
	    	notify.onError({
	    		title:    "Javascript Linter",
	    		subtitle: "Some files didn\'t pass the lint check",
	    		message:  "Check your Terminal to see the errors.",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/js.png',
	    		wait: true,
	    		onLast: true
	    	})(err);
		};

		return gulp.src(basePaths.js + 'theme/*.js')
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(cache('js-linting'))
			.pipe(jshint())
			.pipe(jshint.reporter(stylish))
			.pipe(jshint.reporter('fail'));
	});

	gulp.task('lint:scripts-admin', function () {

		var onError = function(err) {
	    	notify.onError({
	    		title:    "Admin Javascript Linter",
	    		subtitle: "Some files didn\'t pass the lint check",
	    		message:  "Check your Terminal to see the errors.",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/js.png',
	    		wait: true,
	    		onLast: true
	    	})(err);
		};

		return gulp.src(basePaths.js + 'admin/*.js')
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(cache('js-linting'))
			.pipe(jshint())
			.pipe(jshint.reporter(stylish))
			.pipe(jshint.reporter('fail'));
	});

	gulp.task('lint:scripts-shared', function () {

		var onError = function(err) {
	    	notify.onError({
	    		title:    "Shared Javascript Linter",
	    		subtitle: "Some files didn\'t pass the lint check",
	    		message:  "Check your Terminal to see the errors.",
	    		sound:    "Sosumi",
	    		icon: './config/gulp/images/error.png',
	    		contentImage: './config/gulp/images/js.png',
	    		wait: true,
	    		onLast: true
	    	})(err);
		};

		return gulp.src(basePaths.js + 'shared/*.js')
			.pipe(plumber({
				errorHandler: onError
			}))
			.pipe(cache('js-linting'))
			.pipe(jshint())
			.pipe(jshint.reporter(stylish))
			.pipe(jshint.reporter('fail'));
	});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('clean', ['clean:sass', 'clean:sass-admin', 'clean:sass-shared', 'clean:scripts', 'clean:scripts-admin', 'clean:scripts-shared', 'clean:bower']);

	// Used to remove the pre-existing minified css so does not make .min.min.css etc.
	gulp.task('clean:sass', function () {
		return del(basePaths.cssCompiled + '/theme.min.css', basePaths.cssCompiled + 'libraries.min.css', basePaths.cssCompiled + 'libraries.css');
	});

	// Used to remove the pre-existing minified css so does not make .min.min.css etc.
	gulp.task('clean:sass-admin', function () {
		return del(basePaths.cssCompiled + '/admin.min.css');
	});

	// Used to remove the pre-existing minified css so does not make .min.min.css etc.
	gulp.task('clean:sass-shared', function () {
		return del(basePaths.cssCompiled + '/shared.min.css');
	});

	// Used to remove the pre-existing minified js so does not make .min.min.js etc.
	gulp.task('clean:scripts', function () {
		return del(basePaths.js + 'theme.min.js');
	});

	// Used to remove the pre-existing minified js so does not make .min.min.js etc.
	gulp.task('clean:scripts-admin', function () {
		return del(basePaths.js + 'admin.min.js');
	});

	// Used to remove the pre-existing minified js so does not make .min.min.js etc.
	gulp.task('clean:scripts-shared', function () {
		return del(basePaths.js + 'shared.min.js');
	});

	// Used to remove the pre-existing bower components, to ensure uninstalled components are gone
	gulp.task('clean:bower', function () {
		return del(basePaths.installedBower);
	});
		// Used to remove the pre-existing bower fonts, to ensure uninstalled fonts are gone
		gulp.task('clean:bower:fonts', function () {
			return del(basePaths.fonts + '*');
		});

	// Used to remove the pre-existing dist folder
	gulp.task('clean:dist', function () {
		return del(basePaths.dist);
	});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

// This is used to copy over Understraps files at the START of the project ONLY
gulp.task('start-project', ['set-BS-path', 'set-theme-name'], function() {

	////////////////// All Bootstrap 4 Assets /////////////////////////
	// Copy all Bootstrap JS files
	    gulp.src(basePaths.node + 'bootstrap/dist/js/bootstrap.bundle.js')
	       .pipe(gulp.dest('./js/bootstrap4'));

	// Copy all Bootstrap SCSS files
	    gulp.src(basePaths.node + 'bootstrap/scss/**/*.scss')
	       .pipe(gulp.dest('./sass/bootstrap4'));
	////////////////// End Bootstrap 4 Assets /////////////////////////

	// _s JS files
	    gulp.src(basePaths.node + 'undescores-for-npm/js/*.js')
	        .pipe(gulp.dest('./js/understrap'));

	// tether
	    gulp.src(basePaths.node + 'tether/dist/js/tether.js')
	        .pipe(gulp.dest('./js/tether'));
        gulp.src([basePaths.node + 'tether/dist/css/**/*.css', '!*.min.css'])
	       .pipe(gulp.dest('./sass/tether'));
});

	gulp.task('set-BS-path', function() {
		var gulp_path = __dirname;
		var regex = /htdocs\/(.*)/;
		var match = regex.exec(gulp_path);

		if(match === null) {
			return;
		}

		var matches = match[1].split('/');
		var proj_dir = matches[0];

		return gulp.src('./gulpfile.js')
		.pipe(plumber())
		.pipe(replace('localhost/'+'set_path_here/', 'localhost/' + proj_dir + '/'))
		.pipe(gulp.dest('./'));
	});

	gulp.task('set-theme-name', function() {
		var gulp_path = __dirname;
		var regex = /htdocs\/(.*)/;
		var match = regex.exec(gulp_path);

		if(match === null) {
			return;
		}

		var matches = match[1].split('/');
		var theme_name = matches[0];

		return gulp.src('./style.css')
		.pipe(plumber())
		.pipe(replace('&site-name&', theme_name))
		.pipe(gulp.dest('./'));
	});




// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

// Copies the files to the /dist folder for distributon
gulp.task('dist', function(callback) {
  runSequence(['clean:dist', 'bower'], ['sass', 'sass-admin', 'sass-shared', 'scripts', 'scripts-admin', 'scripts-shared'], 'dist:build', callback);
});

	gulp.task('dist:build', function() {

	    return gulp.src([
	    	'*',
	    	'**/*',
	    	'!sass',
	    	'!sass/**/*',
	    	'!bower_components',
	    	'!bower_components/**/*',
	    	'!config',
	    	'!config/**/*',
	    	'!node_modules',
	    	'!node_modules/**/*',
	    	'!src',
	    	'!src/**/*',
	    	'!dist',
	    	'!dist/**/*',
	    	'!.bowerrc',
	    	'!bower.json',
	    	'!gulpfile.js',
	    	'!package.json',
	    	'!css/*.map',
	    	'!package-lock.json',
	    	'!bitbucket-pipelines-sample.yml',
	    	'!proj_root.gitignore',
	    	'!README.md',
	    	'!readme.txt'
	    ])
	    .pipe(plumber())
	    .pipe(notify({
		   title: 'Distribution folder built',
		   message: 'The dist folder is ready for upload.',
		   sound: "Pop",
		   icon: './config/gulp/images/pass.png',
		   contentImage: './config/gulp/images/dist.png',
		   onLast: true
		}))
	    .pipe(gulp.dest(basePaths.dist));
	});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

// Used to bump the version number using command line arguement of 'major', 'minor', 'patch', 'prerelease'
gulp.task('bump', ['bump:style', 'bump:enqueue'], function(){
	return gulp.src('./index.php')
    .pipe(plumber())
    .pipe(notify({
	   title: 'Version number bumped',
	   message: 'The version number in enqueue.php and style.css has been updated.',
	   sound: "Pop",
	   icon: './config/gulp/images/pass.png',
	   onLast: true
	}));
});

	gulp.task('bump:style', function(){
		var args = require('get-gulp-args')();

		var bumpType = args[0];

		if('major' != bumpType && 'minor' != bumpType && 'patch' != bumpType && 'prerelease' != bumpType && 'number' != bumpType) {
			bumpType = 'minor';
		}

		if('number' != bumpType) {
			gulp.src('./style.css')
			.pipe(plumber())
			.pipe(bump({type:bumpType}))
			.pipe(gulp.dest('./'));
		} else {
			gulp.src('./style.css')
			.pipe(plumber())
			.pipe(bump({version: args.version}))
			.pipe(gulp.dest('./'));
		}
	});

	gulp.task('bump:enqueue', function(){
		var args = require('get-gulp-args')();

		var bumpType = args[0];

		if('major' != bumpType && 'minor' != bumpType && 'patch' != bumpType && 'prerelease' != bumpType && 'number' != bumpType) {
			bumpType = 'minor';
		}

		if('number' != bumpType) {
			gulp.src('./inc/enqueue.php')
			.pipe(plumber())
			.pipe(bump({type:bumpType}))
			.pipe(gulp.dest('./inc/'));
		} else {
			gulp.src('./inc/enqueue.php')
			.pipe(plumber())
			.pipe(bump({version: args.version}))
			.pipe(gulp.dest('./inc/'));
		}
	});

// >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< >< ><

gulp.task('default', ['watch']);
