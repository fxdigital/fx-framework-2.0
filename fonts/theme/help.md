# Adding custom fonts

To add custom fonts you will need to generate a font face declaration on a service such a FontSquirrel.

Then you need to put that generated folder in this folder (/fonts/theme).

Finally enqueue the font face stylesheet in enqueue.php in the site_fonts() function, like below:

```
function site_fonts() {

	...

	wp_register_style( 'tin-font', get_stylesheet_directory_uri() . '/fonts/theme/tin/stylesheet.css' );
	wp_enqueue_style( 'tin-font' );
}
```