## About

This is the new FX Framework, based off of Understrap.

## Changelog
	- ** 2.3.0 Apr. 8th 2019 - Fixes some npm things **
		- Updates some npm package versions that are "critical" vulnerability.
		- Fixes the breakpoint definitions as didn't compile on Windows.

	- ** 2.2.2 Aug. 22nd 2018 - Fixes breaking SASS function bug **
		- There was a bug with a missing "@" in the color-map for brand colors that meant that SASS wouldn't build. Whoops!

	- ** 2.2.1 Aug. 1st 2018 - gitignore fix and fix shortcode styling **
		- The JS files for the new admin and shared scripts were being ignored when they shouldn't have been. They are excluded now.
		- The brand-color map wasn't working properly as used by the [social] shortcode.

	- ** 2.2.0 Jul. 6th 2018 - New stylesheets, scripts & libraries  **
		- Added new pairs of stylesheets/ scripts that are loaded in the WP Admin (back-end) and another that is loaded on both the front-end and back-end.
		- Installed FontAwesome and InlineSVG in Bower standard.
			- Updated FontAwesome Bower main file override.
			- Run InlineSVG on jQuery.ready by default.
		- Removed unnecessary WooCommerce templates.
		- Allow BEM class names in Sass linter.
		- Added help guide for loading custom fonts.
		- Updated Bootstrap to 4.1.1.
		- Excluded more files from `gulp dist`.

	- ** 2.1.3 Jun. 4th 2018 - Pipelines and ACF JSON**
		- Set correct npm version in pipelines to prevent crashing due to major update.
		- Added ability to save ACF field groups in JSON as local files that will be committed in the repo. See Nuclino for more info.

	- ** 2.1.2 Apr. 8th 2018 - Small fixes**
		- Updated site credit link to correct URL.
		- Added rel="noopener noreferrer nofollow" to "_blank" site credit link in footer.
		- Added Bitbucket pipelines config file.
		- Cleaned up 404 page.

	- ** 2.1.1 Mar. 8th 2018 - Fix to new Bootstrap version**
		- Set new default Bootstrap version as stable 4.0.0.
		- Got `gulp start-project` to use the bundled.js version of Bootstrap.

	- ** 2.1.0 Nov. 28th 2017 - First major/minor upgrade to FX Framework **
		- This updates includes many useful changes to fix issues and improve dev process from the 2.0 release.
		- Changes:
			- Made theme comptaible with latest Bootstrap (4.0.0-beta-2.0 at point of release).
			- Made theme comptaible with latest Understrap (0.6.6 at point of release).
			- Bootstrap-necessary libraries (now Popper.js rather than Tether.js) are auto bundled with BS's JS.
			- Added utility functions to utilities.php.
			- Made Sass process more granular.
			- Adopted many Bootstrap Sass variables for better control of outputted default styles.

	- ** 2.0.5 Jul. 25th 2017 - Add check for now bower_components**
		- Fix bug in the gulp dist process if no bower_components present.

	- ** 2.0.4 Jul. 17th 2017 - Fixes before use on FX site**
		- Remove trailing comma from overrides in bower.json.
		- Update FX framework repo URLs.

	- ** 2.0.4 May. 24th 2017 - Clayton - Made some small improvements before YP build**
		- Added bower as a node dev dependency.
		- Added meta for the theme color for Android browsers.
		- Added post **uninistall** script to npm.

	- ** 2.0.3 May. 24th 2017 - Made some small improvements**
		- Changed site and page wrapper class names.
		- Added jQuery Throttle/ Debounce as dependency.
		- Updated Woocommerce template function call to new 3.0 syntax.

	- ** 2.0.2 Apr. 10th 2017 - Removed unecessary ACF import code**
		- Now the file is just a placeholder for this content.

	- ** 2.0.1 Apr. 3rd 2017 - Fixing up the framework for pipeline**
		- Some bugs on the pipeline have been fixed.
		- Checks for whether the regex returns anything on start-project as this fails on pipeline.
		- Adds a default bower package (animate.css) so the bower_components folder is made and does not crash dist task.

	- ** 2.0.0 Apr. 3rd 2017 - Fixing up the framework**
		- Fixes bugs from release.
		- Cleans up the theme with new screenshot and Theme details.

	- ** 1.0.2-1.0.4 Jan. 20th 2017 - Introducing the new FX framework**
		- Fixes bugs from last major version release.

	- ** 1.0.1 Jan. 16th 2017 - Introducing the new FX framework**
		- Fixes bugs from last major version release.
		- Changes:
			- Proper placing of $version in `enqueue.php` to prevent scoping errors.
			- Fixes not working Sass browser sync.
			- Includes and queues *Tether*, which is 100% necessary.
			- Added a .gitignore for the project root, to be moved after theme install.

	- ** 1.0.0 Jan. 13th 2017 - Introducing the new FX framework**
		- This is a complete overhaul of the framework, bringing together all the features and squashing all the niggles.
		- This version gives a much cleaner experience to watching files and then running the relevant process.
		- Also improved is the onboarding process when the project is first started.
		- Also **big** improvements to getting external libaries which now requires no editing of the gulpfile to include scripts.
		- Main changes:
			- More refined, more resiliant and more raunchy code building with linters, compilers and notifications for Sass and JS code.
			- A simpler process of adding new libraries, using Bower, which allows the automatic inclusion of the libraries main files (if supported).
			- Automatic code linting for consistent coding styles across the team.
			- Improved 'dist' process that cleans dist folder and compiles Sass and JS automatically before buliding.
			- Automatic version numbering in both `enqueue.php` and `style.css`, using simple gulp task for major, minor or prerelease semantic version numbering.
			- Cleaner and more straighforward gulpfile for future task editing and additions.

	- ** 0.1.3 Nov. 8th 2016 - Fixes bugs from initial commit**
		- Fixes gulp things that didn't work from initial commit.

	- ** 0.1.2 Oct. 31st 2016 - Fixes version numbering**
		- Weird version number, should follow semantic numbering now.

	- ** 0.1 Oct. 31st 2016 - First commit**
		- Initial build of the new framework from FX Day.
		- Have used Understrap 0.5 as base and built from that using previous project's knowledge.
		- Have mainly changed the gulp file which affects how the sass and js are pulled in.
