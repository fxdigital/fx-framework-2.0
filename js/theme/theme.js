// The new "custom.js"
// Not ALL custom JS should live here, try to be modular and separate it out where appropriate.

jQuery(document).ready(function($) {
	if(typeof inlineSVG !== 'undefined') {
		inlineSVG.init({
			svgSelector: 'img.svg', // the class attached to all images that should be inlined
			initClass: 'js-inlinesvg', // class added to <html>
		}, function () {
			// console.log('All SVGs inlined');
		});
	}
});
